from django.shortcuts import render
from .serializer import MangaSerializer
from rest_framework import viewsets
from .models import Manga

class MangaViewSet(viewsets.ModelViewSet):
    queryset = Manga.objects.all()
    serializer_class = MangaSerializer