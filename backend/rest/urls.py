# backend/notes/urls.py
from rest_framework import routers
from .views import MangaViewSet

# Создаем router и регистрируем наш ViewSet
router = routers.DefaultRouter()
router.register(r'manga', MangaViewSet)

# URLs настраиваются автоматически роутером
urlpatterns = router.urls