from django.db import models

class Manga(models.Model):
    title = models.CharField(max_length=256)
    description = models.CharField(max_length=500)

    def __str__(self):
        return str(self.title)